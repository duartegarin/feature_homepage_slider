<?php
/**
 * @file
 * homepage_slider.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function homepage_slider_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function homepage_slider_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_image_default_styles().
 */
function homepage_slider_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_slide
  $styles['homepage_slide'] = array(
    'name' => 'homepage_slide',
    'effects' => array(
      10 => array(
        'label' => 'Dimensionar e recortar',
        'help' => 'Dimensionar e recortar ira manter as proporções da imagem original e posteriormente recortar a dimensão mais larga. Esta função é mais útil para criar miniaturas perfeitamente quadradas sem distorcer a imagem.',
        'effect callback' => 'image_scale_and_crop_effect',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '920',
          'height' => '430',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implementation of hook_node_info().
 */
function homepage_slider_node_info() {
  $items = array(
    'hp_slide' => array(
      'name' => t('HP Slide'),
      'base' => 'node_content',
      'description' => t('Um slide para ser introduzido no Slideshow da Homepage.'),
      'has_title' => '1',
      'title_label' => t('Título do slide'),
      'help' => '',
    ),
  );
  return $items;
}
