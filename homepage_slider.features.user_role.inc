<?php
/**
 * @file
 * homepage_slider.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function homepage_slider_user_default_roles() {
  $roles = array();

  // Exported role: hp-slider
  $roles['hp-slider'] = array(
    'name' => 'hp-slider',
    'weight' => '5',
  );

  return $roles;
}
