<?php
/**
 * @file
 * homepage_slider.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function homepage_slider_user_default_permissions() {
  $permissions = array();

  // Exported permission: create hp_slide content
  $permissions['create hp_slide content'] = array(
    'name' => 'create hp_slide content',
    'roles' => array(
      0 => 'administrator',
      1 => 'hp-slider',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any hp_slide content
  $permissions['delete any hp_slide content'] = array(
    'name' => 'delete any hp_slide content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any hp_slide content
  $permissions['edit any hp_slide content'] = array(
    'name' => 'edit any hp_slide content',
    'roles' => array(
      0 => 'administrator',
      1 => 'hp-slider',
    ),
    'module' => 'node',
  );

  return $permissions;
}
